//
//  Training_AppStartupApp.swift
//  Training-AppStartup
//
//  Created by Tofik Khan on 4/28/21.
//

import SwiftUI

@main
struct Training_AppStartupApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
