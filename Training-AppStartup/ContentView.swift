//
//  ContentView.swift
//  Training-AppStartup
//
//  Created by Tofik Khan on 4/28/21.
//
// Using this to work on a Model-View-Controller learning exercise while following instructions from a LinkedIn Learning (Lynda.com) site. 

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
